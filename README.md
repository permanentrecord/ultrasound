## Operational details
The ultrasound application starts on boot.
Windows will boot into the session using the system credentials listed below.

To begin the scanning process:

 1. Click the button on the ultrasound wand.  This will start the motor in the sensor.
 2. Every single click after that will take an image.
 3. When you're complete, double-click the wand button to stop capturing. This will stop the ultrasound motor.

## Hardware details
* **System board**: [Minnowboard Turbot Dual Core](https://store.netgate.com/Turbot2.aspx)
* **Gyroscope**: [Yocto-3D-V2](http://www.yoctopuce.com/EN/products/usb-position-sensors/yocto-3d-v2)

## File locations
* **Application Location:** C:/Ultrasound/
* **Log Location**: ~/log.txt
* **Capture Location**: ~/captures

## System details
* **Username** : Ultrasound
* **Password**: ultrasound

## Image metadata
Image metadata is stored along each image in a .txt file of the same name, containing the following:
* **Compass**: 136.1: 
* **Gyro**: 0.1: 
* **Tilt**: -1.9: 
* **Accelerometer**: 1.008: 