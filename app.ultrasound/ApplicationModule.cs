﻿using Ninject.Modules;
using lib.ultrasound.task;

namespace app.ultrasound
{
    public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConfigurationTask>().To<ConfigurationTask>();
            Bind<IUltrasoundTask>().To<UltrasoundTask>();
            Bind<ILogTask>().To<LogTask>();
        }
    }
}
