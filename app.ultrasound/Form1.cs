﻿using lib.ultrasound.task;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.ultrasound
{
    public partial class Form1 : Form
    {
        private readonly IUltrasoundTask _ultrasoundTask;

        public Form1(IUltrasoundTask ultrasoundTask, ILogTask logTask)
        {
            logTask.LogEvent("Application Started");

            _ultrasoundTask = ultrasoundTask;
            
            //Initialize the hardware
            _ultrasoundTask.InitDevices();

            InitializeComponent();

            //to minimize window
            this.WindowState = FormWindowState.Minimized;

            //to hide from taskbar
            this.Hide();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.BalloonTipText = "Ultrasound App Still Running";
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }
    }
}
