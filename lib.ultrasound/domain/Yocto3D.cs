﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lib.ultrasound.domain
{
    public class Yocto3D
    {
        public Yocto3D(YTilt tilt, string serialNumber)
        {
            Tilt = tilt;
            SerialNumber = serialNumber;

            Compass = YCompass.FindCompass(SerialNumber + ".compass");
            Accelerometer = YAccelerometer.FindAccelerometer(SerialNumber + ".accelerometer");
            Gyro = YGyro.FindGyro(SerialNumber + ".gyro");
        }

        public string SerialNumber { get; set; }
        public YCompass Compass { get; set; }
        public YAccelerometer Accelerometer { get; set; }
        public YGyro Gyro { get; set; }
        public YTilt Tilt { get; set; }

        public double CompassValue
        {
            get { return Compass.get_currentValue(); }
        }

        public double AccelerometerValue
        {
            get { return Accelerometer.get_currentValue(); }
        }

        public double GyroValue
        {
            get { return Gyro.get_currentValue(); }
        }

        public double TiltValue
        {
            get { return Tilt.get_currentValue(); }
        }
    }
}
