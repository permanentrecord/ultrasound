﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lib.ultrasound.task
{
    public interface IConfigurationTask
    {
        T GetValue<T>(string key, T defaultValue);
        List<string> GetListValue(string key, List<string> defaultValue);
    }

    public class ConfigurationTask : IConfigurationTask
    {
        System.Configuration.AppSettingsReader _reader;
        public ConfigurationTask()
        {
            _reader = new System.Configuration.AppSettingsReader();
        }

        public T GetValue<T>(string key, T defaultValue)
        {
            try
            {
                return (T)_reader.GetValue(key, typeof(T));
            }
            catch
            {
                return defaultValue;
            }
        }

        public List<string> GetListValue(string key, List<string> defaultValue)
        {
            try
            {
                var emails = _reader.GetValue(key, typeof(string));
                var result = emails.ToString().Split(',').ToList<string>();
                return result;
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
