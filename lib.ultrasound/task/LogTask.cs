﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lib.ultrasound.task
{
    public interface ILogTask
    {
        void LogError(string error);
        void LogEvent(string eventMessage);
    }

    /// <summary>
    /// Just a wrapper around logging, in case it changes to a 
    /// windows service later, you can change the implementer of the interface without changing much code.
    /// </summary>
    public class LogTask : ILogTask
    {
        private string logLocation { get; set; }

        public LogTask()
        {
            logLocation = AppDomain.CurrentDomain.BaseDirectory + @"\";
        }

        private void LogMessage(string error)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(logLocation + "log.txt");
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, error);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }

        public void LogError(string errorMessage)
        {
            LogMessage("Error: " + errorMessage);
        }

        public void LogEvent(string eventMessage)
        {
            LogMessage("Event: " + eventMessage);
        }
    }
}
