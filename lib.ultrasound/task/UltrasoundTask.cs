﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Specialized;
using System.Threading;
using System.IO;

using lib.ultrasound.domain;

using Interson.Controls;
using Interson.Imaging;
using ImageExtension;
using Ninject;

namespace lib.ultrasound.task
{
    public interface IUltrasoundTask
    {
        void InitDevices();
    }

    public class UltrasoundTask : IUltrasoundTask
    {
        #region Ultrasound Variables
        ImageControl imgControl = new ImageControl();

        /// <summary>
        /// Dynamic and Anti-Log
        /// </summary>
        int dynamicIndex = 7;  // 5 L.S.
        byte[] dynamicValue = { 30, 35, 40, 45, 50, 55, 60, 65 };

        DateTime lastClicked;

        /// <summary>
        /// bytRawImage will be passed by reference to the Dll to receive the US image
        /// </summary>
        Bitmap bmpUSImage;
        Byte[,] bytRawImage = new byte[ScanConverter.MAX_VECTORS, ScanConverter.MAX_SAMPLES];


        /// <summary>
        /// Holds an Instance of HWControls
        /// </summary>
        HWControls hwControls;
        String probeName;

        /// <summary>
        /// Define the 2 classes for the ScanConverter: Scan2DClass and ScanConverter
        /// </summary>
        bool initDone = true;
        bool scanning = false;
        Scan2DClass scan2D;
        ScanConverter scanConverter;

        int imageHeight = 1000;
        int imageWidth = 800;
        bool leftToRight = true;
        bool upToDown = true;
        int brightness = 0;
        int contrast = 0;

        int[] frequencies;
        int frequencyIndex = 1;
        int depth = 40;
        byte highVoltage = 70;

        string saveLocation = "";
        #endregion

        //Yoctopuce 3D V2 container
        Yocto3D yocto3D;

        private readonly IConfigurationTask _configurationTask;
        private readonly ILogTask _logTask;

        public UltrasoundTask()
        {
            var kernel = new StandardKernel();

            //IoC in windows services aren't very good, so we're breaking the pattern
            _configurationTask = kernel.Get<ConfigurationTask>();
            _logTask = kernel.Get<LogTask>();

            //Now that we've injected the configuration task lets set our configs
            InitConfigurations();
        }

        public void InitDevices()
        {
            try
            {
                hwControls = new HWControls();
                StringCollection probeNames = new StringCollection();
                hwControls.FindAllProbes(ref probeNames);

                /// Check connection
                if (probeNames.Count > 0)
                {
                    //Set the first probe, in theory we should only have one
                    hwControls.FindMyProbe(0);

                    //Get the probe's name.
                    probeName = probeNames[0];
                }

                /// Initialize Scan2D classes and ScanConverter
                scanConverter = new ScanConverter();
                scan2D = new Scan2DClass();

                /// Initialize the hardbutton on the device and attach an event handler
                hwControls.EnableHardButton();
                hwControls.HWButtonTick += new HWControls.HWButtonHandler(WatchHWButton); //Hardware Button was pressed
            }
            catch (Exception ex)
            {
                _logTask.LogError("InitDevices-Ultrasound: " + ex.Message);
            }

            //Start initialization of the Yocto 3D
            // Setup the API to use local USB devices
            string errorMessage = "";

            if (YAPI.RegisterHub("usb", ref errorMessage) != YAPI.SUCCESS)
            {
                _logTask.LogError("RegisterHub error: " + errorMessage);
            }

            YTilt anytilt = YTilt.FirstTilt();
            if (anytilt == null)
            {
                _logTask.LogError("No module connected (check USB cable)");
            } else {
                //Initialize the new container for the Yoctopuce
                try
                {
                    //Get the serial number for further pointers to the device.
                    string serial = anytilt.get_module().get_serialNumber();

                    yocto3D = new Yocto3D(anytilt, serial);
                }
                catch (Exception ex)
                {
                    _logTask.LogError(ex.Message);
                }
            }
        }

        private void WatchHWButton(HWControls HwCtrl, EventArgs e)
        {
            //We're not done initializing so lets not break things.
            if (initDone == false)
                return;

            if (scanning && lastClicked.AddSeconds(1) > DateTime.Now) // Already started then stop
            {
                StopScan();
            }
            else if (scanning) { //Take a picture
                SaveImageAndData();
            }
            else //Start Scanning
            {
                InitScan();
            }

            //Used for simulating double click since we have one button to start/stop/take pictures
            lastClicked = DateTime.Now;
        }

        public void InitScan()
        {
            try
            {
                if (scanning)
                    return;

                scanning = true;

                // Start the  Motor
                HWControls.StartMotor();

                //If we've made it this far, it's safe to start getting device settings from the SDK
                InitHardwareConfigurations();

                /// Initialize bmpUSImage
                bmpUSImage = new Bitmap(imageWidth, imageHeight, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                ///Send frequency to HW
                HWControls.SetFrequency((byte)frequencyIndex, frequencies[frequencyIndex]);
                ///Send High Voltage value to HW
                HWControls.SendHighVoltage(highVoltage);
                ///Enable High Voltage
                HWControls.EnableHighVoltage();
                //Send the dynamic value to HW
                HWControls.SendDynamic(dynamicValue[dynamicIndex]);  // L.S.

                ///Calculate the scanconverter according to the characteristics of the image
                scanConverter.HardInitScanConverter(depth, upToDown, leftToRight, imageWidth, imageHeight);

                scan2D.StartReadScan(ref bytRawImage);
                HWControls.StartBmode(); //start acquisition

                ///Done
                initDone = true;
            }
            catch (Exception ex)
            {
                _logTask.LogError("InitScan: " + ex.Message);
            }
        }

        public void StopScan()
        {
            try
            {
                // Stop the  Motor
                HWControls.StopMotor();

                scanning = false;

                HWControls.StopAcquisition();//stop acquisition
                if (scan2D != null)
                    scan2D.StopReadScan();// stop scan
            }
            catch (Exception ex)
            {
                _logTask.LogError("StopScan: " + ex.Message);
            }
        }

        void SaveImageAndData()
        {
            try
            {
                string fileName = DateTime.Now.ToString("MM-dd-yyyy-hh-dd-ss");

                //Clear the last image in memory since we're passing it as ref.
                bmpUSImage = new Bitmap(imageWidth, imageHeight, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                imgControl.ApplyLUT(bytRawImage);
                scan2D.Build2D(ref bmpUSImage, bytRawImage);
                bmpUSImage.Save(saveLocation + fileName + ".bmp");

                StreamWriter streamWriter = new StreamWriter(saveLocation + fileName + ".txt");
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.AppendFormat("Compass: {0}: ", yocto3D.CompassValue).AppendLine();
                stringBuilder.AppendFormat("Gyro: {0}: ", yocto3D.GyroValue).AppendLine();
                stringBuilder.AppendFormat("Tilt: {0}: ", yocto3D.TiltValue).AppendLine();
                stringBuilder.AppendFormat("Accelerometer: {0}: ", yocto3D.AccelerometerValue).AppendLine();

                streamWriter.Write(stringBuilder.ToString());

                streamWriter.Close();
            }
            catch (Exception ex)
            {
                _logTask.LogError("StopImageAndData: " + ex.Message);
            }
        }

        private void InitConfigurations()
        {
            //Image Configurations

            //Set Image Height
            imageHeight = _configurationTask.GetValue("ImageHeight", 800);
            //Set Image Width
            imageWidth = _configurationTask.GetValue("ImageWidth", 800);
            //Image is left to right
            leftToRight = _configurationTask.GetValue("LeftToRight", true);
            //Image is up to down
            upToDown = _configurationTask.GetValue("UpToDown", true);
            //Brightness is up to down
            brightness = _configurationTask.GetValue("Brightness", 127);
            //Contrast is up to down
            contrast = _configurationTask.GetValue("Contrast", 255);

            //Set our image control for holding the bitmap's config
            imgControl.SetBrightness(brightness);
            imgControl.SetContrast(contrast);


            //Device Configurations

            //Scan Depth
            depth = _configurationTask.GetValue("Depth", 40);

            //High Voltage
            highVoltage = _configurationTask.GetValue<byte>("HighVoltage", 40);

            //Locations
            saveLocation = _configurationTask.GetValue("SaveLocation", "");
        }

        /// <summary>
        /// Calls specific to getting hardware settings from the SDK after we've init'd
        /// the device and confirmed it's there.
        /// </summary>
        private void InitHardwareConfigurations()
        {
            //Frequencies
            frequencies = new int[HWControls.MAX_FREQ];
            frequencies = hwControls.GetFrequency();
            frequencyIndex = _configurationTask.GetValue("FrequencyIndex", 1);
        }
    }
}
