﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lib.ultrasound.task;

namespace service.ultrasound
{
    class NinjectBindings : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IConfigurationTask>().To<ConfigurationTask>().InSingletonScope();
            Bind<IUltrasoundTask>().To<UltrasoundTask>().InSingletonScope();
            Bind<ILogTask>().To<LogTask>().InSingletonScope();
        }
    }
}
