﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.Ninject;

namespace service.ultrasound
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.UseNinject(new NinjectBindings());
                x.Service<UltrasoundService>(sc =>
                {
                    sc.ConstructUsingNinject();
                    sc.WhenStarted((service, hostControl) => service.Start(hostControl));
                    sc.WhenStopped((service, hostControl) => service.Stop(hostControl));
                });
                x.RunAsLocalService();
                x.SetDescription("Ultrasound Service");
                x.SetDisplayName("Ultrasound Service");
                x.SetServiceName("Ultrasound_Service");
            });
        }
    }
}
