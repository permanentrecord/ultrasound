﻿using Ninject;
using System.Reflection;
using lib.ultrasound.task;
using System.Threading;
using Topshelf;
using System.Timers;

namespace service.ultrasound
{
    public partial class UltrasoundService : ServiceControl
    {
        private readonly ConfigurationTask _configurationTask;
        private readonly LogTask _logTask;
        private readonly UltrasoundTask _ultrasoundTask;

        public UltrasoundService()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            //Get the configuration task
            _configurationTask = kernel.Get<ConfigurationTask>();
            //Get the logger task
            _logTask = kernel.Get<LogTask>();
            //Get the Ultrasound task
            _ultrasoundTask = kernel.Get<UltrasoundTask>();

            _ultrasoundTask.InitDevices();
        }

        public bool Start(HostControl hostControl)
        {
            _logTask.LogEvent("Service Started");

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _logTask.LogEvent("Service Stopped");

            return true;
        }
    }
}
